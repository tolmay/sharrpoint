# `sharrpoint` package make log

This objective of this document is to track the history of all steps necessary
to build the `sharrpoint` package. As the process should be quite generic it
can be used for any package. This document can be used as training material.

## Readings & documentation

There are many blog posts and documentation available on R packages, the best
is to start by [R Packages by Hadley Wickham](http://r-pkgs.had.co.nz/) as it 
is well written and complete.


## High level principles

### What is a package

A package is a set of functions, potentially some including some internal data
and datasets and their documentation. 

- R functions are stored in `R/` folder, a good practice is to have one file per
function. 
- Each function should be associated with unit tests stored in `tests` folder.
- Each function and datasets should be well documented using Roxygen2 format

### Package development cycle

Here is a proposed development cycle (@@TODO to be fine tuned) :

1. Design your package principle and functions structure (nothing better that a
pen and a paper at this stage)
2. Start to code your function and store it in `R/` folder, it's better to have
one function per file, think about naming conventions and coding best practices
3. At the same time, design the tests for your function. It's a good practice to
write the testing as you code, this will make your work more reliable
4. When you've done with your function, you should :
    a. Generate documentation : `devtools::document()`
    b. Launch the tests : `devtools::test()`
    c. Iterate until tests are passed
    d. Load you package and play with the functions : `devtools::load_all()`
    e. Another step can be used before your release your package, 
    it's `devtools::check()`. It will check if your packages is compliant with
    Rcran standards
    f. This final step will also generate the vignettes of the package with are
    markdown documents illustrating examples.

### Use of git

It's highly recommended to work with git / gitlab to support this process. 
Use a development branch and a main branch (stable) to merge the package 
development when they are tested.

**Continuous integration :** even better is to confiure CI/CD on gitlab to have
test of the package systematically ran for each commit.



## Let's start : build `sharrpoint` package environment

### Prerequisites

You will need the following packages :

```
library(usethis) # needed by devtools
library(devtools) # all development tools for packages
library(roxygen2) # for documentation
library(testthat) # for testing
```

### Package structure 

If you use Rstudio, you can start a package from scratch using the GUI or just
use `usethis::create_package("../sharrpoint/")`. This will initiate the basic
structure of your package :

- `R/` folder to store your code (it's not possible to have subfolders there)
- `DESCRIPTION` to document the high level information about your package
- `NAMESPACE` which will be updated automatically by roxygen, this includes all 
the dependances of your package

More information about this step can be found
[here](http://r-pkgs.had.co.nz/package.html)

### Testing

We will have now to set-up the testing environment.
```
usethis::use_test()
```
This will create a `test` folder.

### Documentation

It's key to well document your code and your functions. For the later, the best
is to use Roxygen comment formatting. A good reference can be found [here](http://r-pkgs.had.co.nz/man.html).

Basically, we use a specific format of comments to document functions, their
arguments, depencies, output, examples ... `devtools::document()` will then
generate automatically the user manuals and the online documentation your can
get in R console when you type for instance ?read.csv(). Example from Hadley's
book :

```
#' Add together two numbers.
#' 
#' @param x A number.
#' @param y A number.
#' @return The sum of \code{x} and \code{y}.
#' @examples
#' add(1, 1)
#' add(10, 1)
add <- function(x, y) {
  x + y
}
```

### Coding Styling

Some rules (to be discussed) :

- function are in camelCase, based on verbs when possible (e.g. `getToken()`)
- objects are in smallcase, with underscore when necessary (e.g. `my_token`, `folder_list`)
- code intendation is 4 characters
- `for` loops should be avoided a much as possible, except when parsing simple
list with less than 10 objects. Use of `apply` family function instead.
- comment comments should be use to describe "why" code is done like that and
not "what" the code is doing
- description of the function should be done in Roxygen headers
- when calling other packages, import only the function we need and always refer
to the full function name : `data.table::rbindlist()` and `importFrom data.table rbindlist`
- functions with multiple arguments are coded like bellow to ease commenting
when debugging :
```
result <- doSomething(
    argument1 = "value1"
    , argument2 = "Value2"
    , argugment3 = "value3"
    # , argument4 = "example of commented argument"
    argument5 = "last value"
)
```


## Let's make a first iteration, code, test, load

### `getToken()` function

Let's start by the first function of the package, the one to get the
authentication token. We code this function in a separated file in `R/` 
directory.

### Generate documentation

Then let's document the package first 
```
devtools::document()
```

Then you'll have to edit the `DESCRIPTION` file and re-run documentation.

### Unit tests

Finally we code of set of unit test stored in `tests/thesthat/test-getToken.R` checking only the
inputs and the fact that api call fails with error code.

### Manual testing

First you can create a config file (not present on git) with all credentials need

```
client_id <- "xxxx"
client_secret <- "xxxx"
app_title <- "xxxx"
app_domain <- "xxxx"
redirect_uri <- "xxxx"
site <- "xxxx"

host <- "xxxx"
tenant_id <- "xxxx"
url_access <- "xxxx"
principal <-  "xxxx"
```

Then try the function :

```
my_token <- sharrpoint::getToken(
    client_id = client_id
    , client_secret = client_secret
    , host = host
    , site = site
    , tenant_id = tenant_id
    , url_access = url_access
    , principal = principal
    )
my_token$access_token
```

This should return a long string containing the authentication token.


# Using git / gitlab

TO BE WRITTEN HERE

Especially how to work with branches
=======


