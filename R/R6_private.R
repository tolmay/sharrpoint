#' def_renew_token

renew_token <- function(){
    my_query <- list(
        grant_type = "client_credentials",
        client_id = paste0(private$.client_id, "@", private$.tenant_id),
        client_secret = private$.client_secret,
        resource = paste0(private$.principal, "/", host, "@", private$.tenant_id)
    )
    api_url_token <- paste0(
        "https://", private$.url_access, private$.tenant_id, "/tokens/OAuth/2"

    )

    response <- httr::POST(url = api_url_token, body = my_query)
    parsed <- jsonlite::fromJSON(httr::content(response, "text"),simplifyVector = FALSE)


    if (!(response$status_code %in% c(200,201,204))){
        private$.connexion_details <- list(
            "Status_code" = response$status_code,
            "Error_name : " = parsed$error,
            "Error_description"  = parsed$error_description,
            "Error_documentation url" = parsed$error_uri

        )
        private$.connexion_statut <- FALSE
        return(FALSE)

    }else{

        private$.api_token <- parsed$access_token
        private$.api_token_type <- parsed$token_type
        private$.token_exp_time <- parsed$expires_on
        private$.connexion_statut <- TRUE
        private$.connexion_details <- list("token_expiration" = parsed$expires_in)
        private$.api_url <- paste0("https://",host,"/teams/",site,"/_api/")
        return(TRUE)

    }
}



#' estimate_validity_time

estimate_validity_time <- function(){
    as.numeric(private$.token_exp_time) - as.numeric(Sys.time())
}


#' check_token_validity

check_token_validity <- function(){
    if(private$.estimate_validity_time() < 100){
        return(private$.renew_token())
    }else{
        return(TRUE)
    }
}

#' check_request
#' @importFrom purrr map_df flatten
#' @param x httr object from a GET or POST request to the api

check_request <- function(x){

    if(missing(x)){return(NULL)}
    if(httr::http_type(x) != "application/json") {print("API did not return json");return(FALSE)}
    parsing_status <- FALSE
    tryCatch({
        parsed <- jsonlite::fromJSON(
            httr::content(x, "text"),
            simplifyVector = FALSE
        )
        parsing_status <- TRUE
    })
    if((!(x$status_code %in% c(200,201,204))) & parsing_status == TRUE){
        cat(
            paste0(
                "Sharepoint Token API request failed\n"
                ,"Status code : ", httr::status_code(x), "\n"
                ,"Error name : ", parsed$error, "\n"
                ,"Error description :\n",parsed$error_description, "\n"
                ,"Error documentation url : ",parsed$error_uri, "\n"
            )
        )
        return(FALSE)
    }

    if(x$status_code %in% c(200,201,204) & parsing_status == FALSE){print("Parsing has failled...");return(FALSE)}
    if(x$status_code %in% c(200,201,204) & parsing_status == TRUE){
        return(parsed)
    }
}


#' user_information_table

user_information_table <- function(){

    headers = c(
        "Accept" = 'application/json',
        "Authorization" = paste(private$.api_token_type, private$.api_token, sep = " ")

    )

    url <- base::sprintf("%sweb/SiteUserInfoList/items?$select=Id,FirstName,LastName,EMail",
                         private$.api_url)

    res <- httr::GET(url = url, httr::add_headers(.headers=headers))
    if(httr::status_code(res) %in% c(200,201,204)){
        parsed <- private$.check_request(res)
        #return(parsed)
        answer_table <- list_to_df(parsed$value)
        return(answer_table)
    }else{
        return(FALSE)
    }

}

#' def_generate_headers
def_generate_headers <- function(){
    c("Accept"          = 'application/json',
      "Authorization"   = paste(private$.api_token_type, private$.api_token, sep = " "),
      "Cache-Control"   = 'no-cache',
      "Content-Type"    = 'application/json',
      "Host"            = private$.host,
      "Accept-Encoding" = 'gzip, deflate')
}









