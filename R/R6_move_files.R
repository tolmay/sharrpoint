#' move_files
#'
#' Here will be the description
#'
#'
#' @param sp_object a object initialized with sharrpoint initializer / this parameter is used when
#' @param input vector of sharepoint files path
#' @param input_type "ServerrelativeUrl" or "FileId"
#' @param files_names custom list of filenames (mandatory for FileId)
#' @param folders vactor of one or multiple folders to store the files (length must be 1 or as much as the number of files)
#'
#' @import httr
#'
#' @return a confimation vector TRUE/FALSE for files existance
#' @export
#'
#' @examples
#' \dontrun{
#'
#'
#' # A sharrpoit object called my_sharepoint have beeen created before
#'
#' my_files_list <- c("Shared Documents/General/toto.txt","Shared Documents/General/toto2.txt")
#' target_folder <- "Shared Docuements/General/new_folder"
#'
#' move_files(sp_object = my_sharepoint,
#'            input = my_files_list,
#'            input_type = "ServerRelativeUrl",
#'            folders = target_folder
#'            )
#'
#' # or
#'
#' my_sharepoint$move_files(input = my_files_list,
#'                          input_type = "ServerRelativeUrl",
#'                          folders = target_folder
#'                          )
#'
#' }
#'


move_files <- function(sp_object, input, input_type, files_names, folders){
    sp_object$move_files(input, input_type, files_names, folders)
    invisible()
}



#' def_move_files
#' @param input vector of sharepoint files path
#' @param input_type "ServerrelativeUrl" or "FileId"
#' @param files_names custom list of filenames (mandatory for FileId)
#' @param folders vactor of one or multiple folders to store the files (length must be 1 or as much as the number of files)

def_move_files <- function(input,
                           input_type,
                           files_names,
                           folders){

    if(input_type != "ServerRelativeUrl" & input_type != "FileId"){stop("Wrong value provided for input_type must be ServerRelativeurl or FileId ", call. = FALSE)}
    if(input_type == "FileId"){stop("Method for FileId not yet implemented",call. = FALSE)}
    if(!is.vector(input)){stop("Input must be a vector", call. = FALSE)}
    if(missing(folders)){stop("Folders must be provided", call. = FALSE)}
    if(input_type == "FileId" & missing(files_names)){stop("Files_names must be provided for input type FilesId", call. = FALSE)}
    if(length(input) > 1 & length(folders) > 1 & length(folders) != length(input)){
        stop("For multiple inputs, length of folder vector must be 1 or length of input (1 folder for all files or a folder for each file)", call. = FALSE)

    }

    headers = private$.generate_headers()

    if(input_type == "ServerRelativeUrl"){
        #If no cutom files names is provided, get the names from the path (must contain the filesnames at the end of the path)
        if(missing(files_names)){
            files_names <- stringr::str_extract(input, "[ \\w-]+\\..+")
        }

        #Try to fix some common issues with folders path encoding
        folders <- sapply(folders, function(x){
            if(stringr::str_detect(x, private$.api_start_url) == TRUE){
                x <- utils::URLencode(x)
                return(x)
            }else{
                if(substr(x, 1,1) == "/"){x <- substr(x, 2, nchar(x))}                   #remove / if url starts by it
                if(substr(x, nchar(x),nchar(x)) == "/"){x <- substr(x, 1, (nchar(x) -1))}#remove / if url ends by it
                x <- paste0(private$.api_start_url,x)
                x <- utils::URLencode(x)
                print(x)
                return(x)

            }
        })

        mapply(FUN=function(sp_path, namefile,target_folders){
            sp_path <- utils::URLencode(sp_path)
            urls <- base::sprintf("%sweb/GetFileByServerRelativeUrl('%s')/MoveTo(NewUrl='%s/%s',flags=1)",
                                  private$.api_url,
                                  sp_path,
                                  target_folders,
                                  namefile
            )

            #print(urls)
            res <- httr::POST(url=urls, httr::add_headers(.headers=headers))
            print(httr::status_code(res))
            if(httr::status_code(res) %in% c(200,404)){
                return(TRUE)
            }else{
                return(FALSE)
            }

        },
        input,
        files_names,
        folders)
    }
}
