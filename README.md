# sharrpoint

An R package to interact with Sharepoint API (files & lists). 
It's covering sharepoint online as used in corporate account.


## Pre-requisite

We need first to open the API service on our Sharepoint site and manage
authorizations. This process is done in two steps :

1. Authentication : we need to create an "App" for your sharepoint site
and get the credentials
2. Authorization : then, we need to grant authorization for this app.

You'll need to be admin of the sharepoint to proceed.

### Authentication

Let's assume that our sharepoint is located at :
`https://my-company.sharepoint.com/sites/mycoolteam (it can also be https://my-company.sharepoint.com/teams/mycoolteam)`

The App creation url is located at this endpoint `/_layouts/15/appregnew.aspx`
which will make :
```https://my-company.sharepoint.com/sites/mycoolteam/_layouts/15/appregnew.aspx
    or
   https://my-company.sharepoint.com/teams/mycoolteam/_layouts/15/appregnew.aspx
    
```

We generate there :
- `client_id`
- `client_secret`
- Title : `mycoolapi`
- app domain : `localhost`
- redirect uri :  `https://localhost`

Keep these information in a safe place (Using environment variable or key manager).


### Authorization

Next step is to grant permissions to the newly created app.

The App authorization is located at this endpoint `/_layouts/15/appinv.aspx`
which will make :
```https://my-company.sharepoint.com/sites/mycoolteam/_layouts/15/appinv.aspx
    or
   https://my-company.sharepoint.com/teams/mycoolteam/_layouts/15/appinv.aspx
    
```

Input your App Id (client_id) to look for the app.

We have to set the "Write" permission by entering the below permission request 
in XML format.

```
<AppPermissionRequests AllowAppOnlyPolicy="true">
<AppPermissionRequest Scope="https://my-company.sharepoint.com/sites/mycoolteam/" Right="FullControl"/></AppPermissionRequests>
```

You can click on `Trust` to complete the process


## Information about this package
	
This package is co-developped by Thibault Mailfait & Barthélémy Longueville.
We used this project as a teaching platform as well, more information regarding
the various steps followed to make this package are listed in the file 
[MAKELOG.md](./MAKELOG.md).


## References

Another package exists but is not covering o365 integration :
https://github.com/LukasK13/sharepointr

Microsoft have a very bad online documentation except on github :
https://github.com/SharePoint/sp-dev-docs/blob/master/docs/sp-add-ins/get-to-know-the-sharepoint-rest-service.md
